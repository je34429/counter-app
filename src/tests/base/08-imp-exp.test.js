import { getHeroeById, getHeroesByOwner } from '../../base/08-imp-exp';
import heroes from '../../data/heroes';
describe('Pruebas en funciones de Héroes', () => {
    test('debe retornar un héroe por id', ()=>{
        const id = 1;
        const heroe = getHeroeById(id);
        const heroeData = heroes.find( h => h.id === id );
        expect( heroe ).toEqual(heroeData);
    });

    test('debe retornar undefined si el heroe no existe', ()=>{
        const id = 100;
        const heroe = getHeroeById(id);

        const heroeData = heroes.find( h => h.id === id );
        expect( heroe ).toBe(undefined);
    });

    //debe retornar un arreglo con los heroes de DC
    //owner
    //toEqual al arreglo filtrado
    test('debe retornar un arreglo con los heroes de DC',()=>{
        const owner = "DC";
        const heroes = getHeroesByOwner(owner);
        const heroesExpected = [
            {
                id: 1,
                name: 'Batman',
                owner: 'DC'
            },
            
            {
                id: 3,
                name: 'Superman',
                owner: 'DC'
            },
            {
                id: 4,
                name: 'Flash',
                owner: 'DC'
            },
            
        ];
        expect( heroes ).toEqual( heroesExpected );
    });

    //debe retornar un arreglo con los heroes de Marvel
    // length = 2 //toBe
    test('debe retornar un arreglo con los heroes de Marvel',()=>{
        const owner = "Marvel";
        const heroes = getHeroesByOwner(owner);
        const heroesExpected = 2;
        expect( heroes.length ).toBe( heroesExpected );
    });

});