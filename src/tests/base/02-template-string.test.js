import '@testing-library/jest-dom';
import { getSaludo } from '../../base/02-template-string';
describe('pruebas en 02-template-string.js', ()=>{

    test('getSaludo debe retornar hola nombre', ()=>{
        const nombre = 'jaime';
        const saludo = getSaludo( nombre );

        expect( saludo ).toBe( 'Hola ' + nombre);
    });

    test('getSaludo debe retornar hola Carlos si no hay argumento en el nombre', ()=>{
        const saludo = getSaludo();

        expect( saludo ).toBe( 'Hola Carlos');
    });


});