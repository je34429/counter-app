import React from 'react';
import PrimeraApp from "../PrimeraApp";
//import { render } from '@testing-library/react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom'

describe('pruebas en <PrimeraApp/>', ()=>{

    /*test('debe mostrar el mensaje "Hola soy Goku"',() => {

        const saludo = 'Hola, soy Goku';
        const { getByText } = render( <PrimeraApp saludo = { saludo } /> );
        expect( getByText( saludo ) ).toBeInTheDocument();

    });*/

    test('debe mostrar <primeraApp/> correctamente',() => {
        const saludo = "oe!!";
        const wrapper = shallow(<PrimeraApp saludo={saludo}/>);
        expect( wrapper ).toMatchSnapshot();
    });


    test('debe mostrar el subtitulo enviado por props', () => {
        const saludo ="epa!"
        const subTitulo = "soy un subtitulo";

        const wrapper = shallow( 
            <PrimeraApp
                saludo = {saludo}
                subtitulo = { subTitulo }    
            />
        );

        const textoParrafo = wrapper.find('p').text();
        //console.log(textoParrafo);
        expect( textoParrafo ).toBe( subTitulo );
    });

});