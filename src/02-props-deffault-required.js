import React from 'react';
import PropTypes from 'prop-types';
//import React, { Fragment } from 'react'

const PrimeraApp = ( { saludo, subtitulo } ) => {
   
    return (
            <>
                <div>{ saludo }</div>
                <p>{ subtitulo }</p>
            </>
            
        );
}

PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired,
}

PrimeraApp.defaultProps = {
    subtitulo: "soy un subtitulo"
}

export default PrimeraApp;