import {retornaArreglo} from '../../base/07-deses-arr'

describe('pruebas en desestructuración', ()=>{
    test('debe retornar un arreglo', () => {     
        const arreglo = retornaArreglo();
        expect(arreglo).toEqual([
            'ABC', 
            123
        ]);
    });

    test('debe retornar un numero y un arreglo', () => {       
        const [ palabra, numero ] = retornaArreglo();
        expect(typeof palabra).toBe('string');
        expect(typeof numero).toBe('number');
    });
    
});