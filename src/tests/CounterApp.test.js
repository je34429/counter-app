import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import CounterApp from '../CounterApp';




describe('pruebas para el <counterApp/>', () => {
    const val =10;
    let wrapper = shallow(<CounterApp value={val} /> );
    beforeEach(()=>{
        wrapper = shallow(<CounterApp value={val}/>);
    });
    

    test('chequeo de counterApp con snapshot valor por defecto 10', () => {
        console.log(wrapper);
        expect(wrapper).toMatchSnapshot();
    });


    test('el valor del contador debe ser 100', () => {
        const val =100;
        const wrapper = shallow(<CounterApp value={val}/>);
        const valorCont = parseInt(wrapper.find('h2').text());
        expect(valorCont).toBe( val );
    });

    test('debe incrementar cuando haga clic sobre +1', () => {
        wrapper.find('button').at(0).simulate('click');
        const valorCont = parseInt(wrapper.find('h2').text());
        expect( valorCont ).toBe( val+1 );
    });

    test('debe disminuir cuando haga clic sobre -1', () => {
        wrapper.find('button').at(2).simulate('click');
        const valorCont = parseInt(wrapper.find('h2').text());
        expect( valorCont ).toBe( val-1 );
    });

    test('debe volver al default cuando haga clic sobre reset', () => {
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(1).simulate('click');
        const valorCont = parseInt(wrapper.find('h2').text());
        expect( valorCont ).toBe( val );
    });

});

