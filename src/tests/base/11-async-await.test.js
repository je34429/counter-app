import { getImagen } from '../../base/11-async-await';

describe('pruebas con async-await y Fetch',()=>{

    test('debe retornar el url de la imagen', async()=>{
        const url = await getImagen();
        //console.log(url);
        const protocol = url.includes('http');
        expect( protocol ).toBe( true );
    });

});