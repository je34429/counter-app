
import React, { useState } from 'react'
import PropTypes from 'prop-types'


const CounterApp = ({value}) =>{
    
    //const state = useState('Goku');
    const [ counter, setCounter ] = useState( value );

    const contar = (a) => {
        setCounter( counter + 1);
    }

    const restar = (a) => {
        setCounter( counter - 1);
    }

    const reset = () => {
        setCounter( value );
    }

    return  <>
                <h1>CounterApp</h1>
                <h2>{ counter }</h2>

                <button className="circ" onClick = { contar }>+1</button>
                <button onClick = { reset }>Reset</button>
                <button className="circ" onClick = { restar }>-1</button>
            </>
}



CounterApp.propTypes = {
    value:PropTypes.number.isRequired,
}

export default CounterApp;