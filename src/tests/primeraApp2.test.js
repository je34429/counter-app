import React from 'react';
import PrimeraApp from "../PrimeraApp";
import { render, screen } from '@testing-library/react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom'

describe('pruebas en <PrimeraApp/>', ()=>{

    const saludo = 'Hola qué tal';
    const subtitulo = 'soy un subtitulo';

    test('debe hacer match con el snapshot', () => { 
        const {container}= render(<PrimeraApp saludo={saludo}/>);
        expect(container).toMatchSnapshot();

    });

    test('debe coincidir el saludo', () => { 
        render(<PrimeraApp saludo={saludo}/>);
        expect(screen.getByText(saludo)).toBeTruthy();
        //screen.debug();
    });

    test('debe de mostrar el titulo en un h1 ', () => { 
        render(<PrimeraApp saludo={saludo}/>);
        expect(screen.getByRole('heading', {level:1}).innerHTML).toContain(saludo);

     });

     test('debe de mostrar el subtitulo ', () => { 
        render(<PrimeraApp subtitle={subtitulo}/>);
        expect(screen.getByText(subtitulo)).toBeTruthy();

     });

});